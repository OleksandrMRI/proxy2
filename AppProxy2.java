package ua.shpp.proxy2;

import ua.shpp.abstracts.MovieInterface;
import ua.shpp.classes.ProxyMovie;

public class AppProxy2 {
    public static void main(String[] args) {
        MovieInterface movie = new ProxyMovie("Rembo", 10);
        movie.showMovie();
        movie = new ProxyMovie("Red hat", 8);
        movie.showMovie();
    }
}